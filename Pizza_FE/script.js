/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1";
const gCONTENT_TYPE = "application/json;charset=UTF-8";
// các combo Pizza
const gCOMBO_SMALL = "Small";
const gCOMBO_MEDIUM = "Medium";
const gCOMBO_LARGE = "Large";

//các loại pizza
const gTYPE_HAWAI = "hawai";
const gTYPE_SF = "seafood";
const gTYPE_BBQ = "BBQ";


var gSelectedComboStructure = { menuName: "", duongKinh: "", suonNuong: "", salad: "", drink: "", price: 0 }
var gSelectPizzaType = "";
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();

  //add sự kiện cho button chọn Size S
  $("#s-select-btn").click(function () {
    onBtnSelectSClick();
  })

  //add sự kiện cho button chọn Size M
  $("#m-select-btn").click(function () {
    onBtnSelectMClick();
  })

  //add sự kiện cho button chọn Size L
  $("#l-select-btn").click(function () {
    onBtnSelectLClick();
  })

  //add sự kiện cho button chọn Pizza Seafood
  $("#seafood-select-btn").click(function () {
    onBtnSelectSFClick();
  })

  //add sự kiện cho button chọn Pizza Hawai
  $("#hawai-select-btn").click(function () {
    onBtnSelectHWClick();
  })

  //add sự kiện cho button chọn Pizza BBQ
  $("#bbq-select-btn").click(function () {
    onBtnSelectBBQClick();
  })

  //gán sự kiện khi nhấn nút Send
  $("#check-order").click(function () {
    onCheckOrderClick();
  })
  //gán sự kiện cho nút confirm trên modal
  $("#confirm-order-modal").on("click", "#btn-confirm-order", function () {
    onConfirmOrderModalClick(this);
  });

});







/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  callAPIAndLoadDrinkSelect();

}
// Hàm thực thi khi Size S được chọn (click)
//input click button Chọn
//output gọi hàm changSelectBtnColor đổi màu button tương ứng
function onBtnSelectSClick() {
  //đổi màu thẻ đã chọn
  $('#s-select-btn').css('background-color', 'coral');
  $('#s-select-btn').parent().siblings(".card-header").removeClass("bg-warning");
  $('#s-select-btn').parent().siblings(".card-header").css('background-color', 'coral');

  $('#m-select-btn').css('background-color', '');
  $('#m-select-btn').addClass("btn btn-warning");
  $('#m-select-btn').parent().siblings(".card-header").addClass("bg-warning");

  $('#l-select-btn').css('background-color', '');
  $('#l-select-btn').addClass("btn btn-warning");
  $('#l-select-btn').parent().siblings(".card-header").addClass("bg-warning");

  console.log("Button Size S được click");
  gSelectedComboStructure = getCombo(gCOMBO_SMALL, 20, 2, "200gr", 2, 150000);
  gSelectedComboStructure.displayInConsoleLog();
}



// Hàm thực thi khi Size M được chọn (click)
function onBtnSelectMClick() {
  //đổi màu thẻ đã chọn
  $('#m-select-btn').css('background-color', 'coral');
  $('#m-select-btn').parent().siblings(".card-header").removeClass("bg-warning");
  $('#m-select-btn').parent().siblings(".card-header").css('background-color', 'coral');

  $('#s-select-btn').css('background-color', '');
  $('#s-select-btn').addClass("btn btn-warning");
  $('#s-select-btn').parent().siblings(".card-header").addClass("bg-warning");

  $('#l-select-btn').css('background-color', '');
  $('#l-select-btn').addClass("btn btn-warning");
  $('#l-select-btn').parent().siblings(".card-header").addClass("bg-warning");

  console.log("Button Size M được click");
  gSelectedComboStructure = getCombo(gCOMBO_SMALL, 20, 2, "200gr", 2, 150000);
  gSelectedComboStructure.displayInConsoleLog();
}


// Hàm thực thi khi Size M được chọn (click)
function onBtnSelectLClick() {
  //đổi màu thẻ đã chọn
  $('#l-select-btn').css('background-color', 'coral');
  $('#l-select-btn').parent().siblings(".card-header").removeClass("bg-warning");
  $('#l-select-btn').parent().siblings(".card-header").css('background-color', 'coral');

  $('#s-select-btn').css('background-color', '');
  $('#s-select-btn').addClass("btn btn-warning");
  $('#s-select-btn').parent().siblings(".card-header").addClass("bg-warning");

  $('#m-select-btn').css('background-color', '');
  $('#m-select-btn').addClass("btn btn-warning");
  $('#m-select-btn').parent().siblings(".card-header").addClass("bg-warning");

  console.log("Button Size L được click");
  gSelectedComboStructure = getCombo(gCOMBO_SMALL, 20, 2, "200gr", 2, 150000);
  gSelectedComboStructure.displayInConsoleLog();
}


// Hàm thực thi khi Type Seafood được chọn (click)
function onBtnSelectSFClick() {
  //đổi màu thẻ đã chọn
  $('#seafood-select-btn').css('background-color', 'coral');
  $('#seafood-select-btn').parent().siblings(".card-body").css('background-color', 'coral');

  $('#hawai-select-btn').css('background-color', 'rgba(124, 25, 25, 0.795)');
  $('#hawai-select-btn').parent().siblings(".card-body").css('background-color', '');

  $('#bbq-select-btn').css('background-color', 'rgba(124, 25, 25, 0.795)');
  $('#bbq-select-btn').parent().siblings(".card-body").css('background-color', '');

  gSelectPizzaType = gTYPE_SF;
  console.log("Button Chọn Pizza " + gSelectPizzaType + " được click");

}

// Hàm thực thi khi Type Hawai được chọn (click)
function onBtnSelectHWClick() {
  //đổi màu thẻ đã chọn
  $('#hawai-select-btn').css('background-color', 'coral');
  $('#hawai-select-btn').parent().siblings(".card-body").css('background-color', 'coral');

  $('#seafood-select-btn').css('background-color', 'rgba(124, 25, 25, 0.795)');
  $('#seafood-select-btn').parent().siblings(".card-body").css('background-color', '');

  $('#bbq-select-btn').css('background-color', 'rgba(124, 25, 25, 0.795)');
  $('#bbq-select-btn').parent().siblings(".card-body").css('background-color', '');

  gSelectPizzaType = gTYPE_HAWAI;
  console.log("Button Chọn Pizza " + gSelectPizzaType + " được click");
}

// Hàm thực thi khi Type BBQ được chọn (click)
function onBtnSelectBBQClick() {
  //đổi màu thẻ đã chọn
  $('#bbq-select-btn').css('background-color', 'coral');
  $('#bbq-select-btn').parent().siblings(".card-body").css('background-color', 'coral');

  $('#seafood-select-btn').css('background-color', 'rgba(124, 25, 25, 0.795)');
  $('#seafood-select-btn').parent().siblings(".card-body").css('background-color', '');

  $('#hawai-select-btn').css('background-color', 'rgba(124, 25, 25, 0.795)');
  $('#hawai-select-btn').parent().siblings(".card-body").css('background-color', '');

  gSelectPizzaType = gTYPE_BBQ;
  console.log("Button Chọn Pizza " + gSelectPizzaType + " được click");
}

// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onCheckOrderClick() {
  console.log("Button check Order được click")
  //định nghĩa đối tượng dữ liệu
  var vThongTinOrderObj = {
    menuDuocChon: null,
    loaiPizza: "",
    hoVaTen: "",
    email: "",
    dienThoai: "",
    diaChi: "",
    loiNhan: "",
    voucher: "",
    loainuoc: "",
    phanTramGiamGia: 0,
    priceVND: 0
  };
  //1 đọc các giá trị input từ màn hình để lưu vào vThongTinOrderObj
  readDataFromWeb(vThongTinOrderObj);
  console.log("%c Đọc thành công", "color: red");
  console.log(vThongTinOrderObj);

  //2: kiểm tra dữ liệu
  var vDataInputCheck = checkInputData(vThongTinOrderObj);  //kiểm tra chọn combo và loại
  if (vDataInputCheck == true) {
    //3. Xử lý hiện thị  
    console.log("%c Send Data", "color:orange");
    showOrderInfor(vThongTinOrderObj);
  }
  return vThongTinOrderObj;
}

//hàm đọc data từ web ReadDataFromWeb
//input param Obj thông tin order
//ouput Obj thông tin order chứa các giá trị đã nhập trên màn hình
function readDataFromWeb(paramThongTinOrderObj) {
  //truy vấn láy thông tin size được chọn
  paramThongTinOrderObj.menuDuocChon = gSelectedComboStructure.menuName;
  paramThongTinOrderObj.loaiPizza = gSelectPizzaType;

  //truy vấn và lấy giá trị trên form nhập
  paramThongTinOrderObj.hoVaTen = $("#inp-fullname").val().trim();
  paramThongTinOrderObj.email = $("#inp-email").val().trim();
  paramThongTinOrderObj.diaChi = $("#inp-dia-chi").val().trim();
  paramThongTinOrderObj.dienThoai = $("#inp-dien-thoai").val().trim();
  paramThongTinOrderObj.loiNhan = $("#inp-message").val().trim();
  paramThongTinOrderObj.loainuoc = $("#drink-select").val();

  paramThongTinOrderObj.voucher = $("#inp-voucher-id").val().trim();
  var vInputVoucherID = paramThongTinOrderObj.voucher;
  debugger;
  //lấy giá trị %giảm giá
  if (vInputVoucherID === "") {
    paramThongTinOrderObj.phanTramGiamGia = 0;
    paramThongTinOrderObj.priceVND = gSelectedComboStructure.price
  }
  if (vInputVoucherID != "") {  //trường hợp input voucher khác rỗng thì check
    var vPercent = callAPIToGetPerByVoucherID(vInputVoucherID);
    paramThongTinOrderObj.phanTramGiamGia = vPercent;
    paramThongTinOrderObj.priceVND = tinhTienSauKhiGiamGia(gSelectedComboStructure.price, vPercent)
  }
}



//hàm gọi ra khi nhấn nút Cofirm Order trên Modal

function onConfirmOrderModalClick() {
  "use strict";
  console.log("NHẤN NÚT CONFIRM MODAL")
  $("#confirm-order-modal").modal("hide");
  $("#your-order-confirm-modal").modal("show");

  var vThongTinOrderObj = onCheckOrderClick();
  var vObjectRequest = {
    kichCo: gSelectedComboStructure.menuName,
    duongKinh: gSelectedComboStructure.duongKinh,
    suon: gSelectedComboStructure.suonNuong,
    salad: gSelectedComboStructure.salad,
    loaiPizza: gSelectPizzaType,
    idVourcher: vThongTinOrderObj.voucher,
    idLoaiNuocUong: vThongTinOrderObj.loainuoc,
    soLuongNuoc: gSelectedComboStructure.drink,
    hoTen: vThongTinOrderObj.hoVaTen,
    thanhTien: vThongTinOrderObj.menuDuocChon.price,
    email: vThongTinOrderObj.email,
    soDienThoai: vThongTinOrderObj.dienThoai,
    diaChi: vThongTinOrderObj.diaChi,
    loiNhan: vThongTinOrderObj.loiNhan,
  }
  callAPIGetDataOrder(vObjectRequest);


}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm getCombo
//input: tham số truyền vào là thuộc tính của đối tượng tương ứng
//output :trả lại một đối tượng combo được tham số hóa
function getCombo(paramMenuName, paramDuongKinhCm, paramSuonNuong, paramSaladGr, paramDrink, paramPriceVnd) {
  gSelectedComboStructure = {
    menuName: paramMenuName,
    duongKinh: paramDuongKinhCm,
    suonNuong: paramSuonNuong,
    salad: paramSaladGr,
    drink: paramDrink,
    price: paramPriceVnd,

    displayInConsoleLog() {
      console.log("%cCOMBO SELECTED - combo pizza đã chọn..........", "color:blue");
      console.log(this.menuName);  //this = "đối tượng này" 
      console.log("Đường kính: " + this.duongKinh);
      console.log("Sườn nướng: " + this.suonNuong);
      console.log("Salad: " + this.salad);
      console.log("Drink" + this.drink);
      console.log("Price:" + "VND " + this.price);
    }
  }
  return gSelectedComboStructure;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
}

//hàm tính tiền sau khi giảm giá
function tinhTienSauKhiGiamGia(paramMenuDuocChon, paramPhanTramGiamGia) {
  var vTienGiamGia = 0;
  if (paramPhanTramGiamGia != 0) {
    vTienGiamGia =
      paramMenuDuocChon -
      (paramMenuDuocChon * paramPhanTramGiamGia) / 100;
  } else {
    vTienGiamGia = paramMenuDuocChon;
  }
  return vTienGiamGia;
}

//hàm check Input data order
function checkInputData(paramThongTinDangKyObj) {
  if (paramThongTinDangKyObj.menuDuocChon === "") {
    alert("Chưa chọn Size Pizza");
    return false;
  }

  if (paramThongTinDangKyObj.loaiPizza == "") {
    alert("Chưa chọn loại pizza");
    return false;
  }

  if (paramThongTinDangKyObj.hoVaTen === "") {
    alert("Chưa nhập họ và tên");
    return false;
  }

  if (validateEmailCheck(paramThongTinDangKyObj.email) == false) {
    return false;
  }

  if (validateTELCheck(paramThongTinDangKyObj.dienThoai) == false) {
    return false;
  }

  if (paramThongTinDangKyObj.diaChi === "") {
    alert("Chưa nhập địa chỉ");
    return false;
  }

  if (paramThongTinDangKyObj.loainuoc === "all") {
    alert("Please choose Drink!");
    return false;
  }

  return true;
}


//hàm check validate email
function validateEmailCheck(paramEmail) {
  if (paramEmail === "") {
    alert("Chưa nhập email");
    return false;
  }

  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  var vInputEmail = document.getElementById("inp-email");
  if (vInputEmail.value.match(mailformat)) {
    return true;
  }
  else {
    alert("Invalid Email address!");
    return false
  }
}

//hàm check số điện thoại
function validateTELCheck(paramTEL) {
  if (paramTEL === "") {
    alert("Chưa nhập Số điện thoại");
    return false;
  }

  if (isNaN(paramTEL)) {
    alert("Số điện thoại phải là số");
    return false;
  }

  return true;
}


function showOrderInfor(vThongTinOrderObj) {
  $("#confirm-order-modal").modal("show");
  $("#input-name").val(vThongTinOrderObj.hoVaTen);
  $("#input-phone").val(vThongTinOrderObj.dienThoai);
  $("#input-address").val(vThongTinOrderObj.diaChi);
  $("#input-message").val(vThongTinOrderObj.loiNhan);
  $("#input-voucher-id").val(vThongTinOrderObj.voucher);

  var vOrderDetail = "Xác nhận: " + vThongTinOrderObj.hoVaTen + "," + vThongTinOrderObj.dienThoai + "," + vThongTinOrderObj.diaChi + "\r\n"
    + "Combo: " + gSelectedComboStructure.menuName + "\r\n"
    + "Size: " + gSelectedComboStructure.duongKinh + "\r\n"
    + "Grilled rib: " + gSelectedComboStructure.suonNuong + "\r\n"
    + "Salad: " + gSelectedComboStructure.salad + "\r\n"
    + "Drink: " + gSelectedComboStructure.drink + "\r\n"
    + "Drink Type: " + vThongTinOrderObj.loainuoc + "\r\n"
    + "--------------------------------------" + "\r\n"
    + "Pizza Type: " + gSelectPizzaType + "\r\n"
    + "Voucher: " + vThongTinOrderObj.voucher + "\r\n"
    + "Price vnd: " + gSelectedComboStructure.price + "\r\n"
    + "Discount: " + vThongTinOrderObj.phanTramGiamGia + "\r\n"
    + "Pay amount vnd: " + vThongTinOrderObj.priceVND;

  $("#details-area").val(vOrderDetail);
}

//hàm gọi API push Order
function callAPIGetDataOrder(paramOrderObj) {
  //insert order
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
    type: "POST",
    contentType: gCONTENT_TYPE,
    data: JSON.stringify(paramOrderObj),
    success: function (paramRes) {
      // B4: xử lý front-end
      handleInsertCourseSuccess(paramRes)
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}


// hàm xử lý hiển thị front-end khi thêm course thành công
function handleInsertCourseSuccess(paramResponseOrder) {
  $("#your-order-confirm-modal").modal("show");
  var vOrderCode = paramResponseOrder.orderCode;
  $("#confirm-order-code-modal").val(vOrderCode);
}


//gọi hàm tính % giảm giá
function callAPIToGetPerByVoucherID(paramVoucherID) {
  "use strict";
  var vPercent = 0;
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail" + "/" + paramVoucherID,
    dataType: "json",
    type: "GET",
    async: false,
    success: function (paramRes) {
      console.log(paramRes);
      vPercent = paramRes.phanTramGiamGia;
      console.log(paramRes);
      return vPercent;
    },
    error: function (paramErr) {
      console.log("Không tìm thấy voucher" + paramErr);
      alert("Không tồn tại mã giảm giá");
    }
  });
  return vPercent;
}

//hàm gọi API select drink
function callAPIAndLoadDrinkSelect() {
  "use strict";
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
    dataType: "json",
    type: "GET",
    success: function (paramRes) {
      console.log(paramRes)
      handleDrinkList(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}

//hàm tạo option cho select drink
function handleDrinkList(paramDrink) {
  "use strict";
  for (var bI = 0; bI < paramDrink.length; bI++) {
    $("#drink-select").append($("<option>", {
      text: paramDrink[bI].tenNuocUong,
      value: paramDrink[bI].maNuocUong
    }));
  }
}




